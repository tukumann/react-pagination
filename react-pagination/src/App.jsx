import './App.css';
import axios from 'axios';
import { useEffect, useState } from 'react';

function App() {
  const [photos, setPhotos] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [fetching, setFetching] = useState(true);
  const [totalCount, setTotalCount] = useState(0);



  useEffect(() => {
    if(fetching) {
      // console.log("fetching");
      axios.get(`https://jsonplaceholder.typicode.com/photos?_limit=10&_page=${currentPage}`)
      .then(res => {
        setPhotos([...photos.splice(photos.length-10, 10), ...res.data]);
        setCurrentPage(prevState => prevState + 1);
        // console.log(res.headers);
        setTotalCount(+res.headers['x-total-count']);
      })
          .finally(() => setFetching(false)) 
    }
  }, [fetching])
  
  useEffect(() => {
    document.addEventListener('scroll', scrollHandler)
    return function() {
      document.removeEventListener('scroll', scrollHandler)
    }
  }, [])

  const scrollHandler = (e) => {
    if (e.target.documentElement.scrollHeight-(window.innerHeight + e.target.documentElement.scrollTop) < 1000
    && photos.length <= totalCount) {
      // console.log(e.target.documentElement.scrollHeight-(window.innerHeight + e.target.documentElement.scrollTop));
      console.log("Let's fetch one more chunk!");
      setFetching(true);
      console.log(fetching);
    }

    console.log('scrollHeight', e.target.documentElement.scrollHeight);
    console.log('scrollTop', e.target.documentElement.scrollTop);
    console.log('innerHeight', window.innerHeight);
  }

  return ( 
    <div className="App">
      
      {photos.map(photo => 
          <div key={photo.id} className='photo'>
              <div><b><p>{photo.id}. {photo.title}</p></b></div>
              <img src={photo.thumbnailUrl} alt='' />
          </div>
        )}

    </div>
  );
}

export default App;
